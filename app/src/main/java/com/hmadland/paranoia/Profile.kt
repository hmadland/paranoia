package com.hmadland.paranoia
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_profile.*
import org.json.JSONObject
import com.android.volley.AuthFailureError
import com.android.volley.Response.Listener
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class Profile : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        //get token from login
        val token = intent.getStringExtra("token")
        //get email to find PlayerID in sharedPref
        val email = intent.getStringExtra("email")
        //get PlayerID from sharedPref
        val mypref = getSharedPreferences(email, Context.MODE_PRIVATE)

        var PlayerID = ""

        val progressBar: ProgressBar = this.progressBar1

            // task is run on a thread
            Thread(Runnable {
                // performing operation
                try {
                    var url = "https://elimination.azurewebsites.net/api/Players/GetPlayerInfo?email=" + email
                    val que = Volley.newRequestQueue(this@Profile)

                    val jsonObjectRequest = JsonObjectRequest(
                            Request.Method.GET, url, null,
                            Listener<JSONObject> { response ->
                                //round photo
                                Glide.with(this)
                                        .load(response.get("PhotoUrl"))
                                        .apply(RequestOptions.circleCropTransform())
                                        .into(ProfilePic)

                                //get UserName
                                userName.text = response.get("UserName").toString()
                                //get profile
                                profile.text = response.get("Profile").toString()

                                PlayerID = response.get("PlayerID").toString()
                            },
                            Response.ErrorListener { error -> Log.e("error is ", "" + error)
                            }
                    )

                    //This is for Headers If Needed
                    @Throws(AuthFailureError::class)
                    fun getHeaders(): Map<String, String> {
                        val params = HashMap<String, String>()
                        params.put("Content-Type", "application/json; charset=UTF-8")
                        params.put("token", token)
                        return params
                    }
                    que.add(jsonObjectRequest)

                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

                while(ProfilePic.getDrawable() == null){

                }
                this@Profile.runOnUiThread(java.lang.Runnable {
                    progressBar1.visibility = View.GONE
                })
            }

            ).start()

        ///////////buttons to other views//////////////////////////////
        //go to current target view
        CurrentGame.setOnClickListener{
            val intent = Intent(this, Target::class.java)
            intent.putExtra("token", token)
            intent.putExtra("email", email)
            intent.putExtra("PlayerID", PlayerID)
            startActivity(intent)
        }

        Inventory.setOnClickListener{
            val intent = Intent(this, com.hmadland.paranoia.Inventory::class.java)
            startActivity(intent)
        }

        coming.setOnClickListener{
            val intent = Intent(this, UpcomingGames::class.java)
            startActivity(intent)
        }
        ////////////////////////////////////////////////////////////////////
    }
}
