package com.hmadland.paranoia

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_target.*
import org.json.JSONObject

class Target : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_target)

        //get token from login
        val token = intent.getStringExtra("token")
        //get email to find PlayerID in sharedPref
        val email = intent.getStringExtra("email")

        val PlayerID = intent.getStringExtra("PlayerID")
        //get PlayerID from sharedPref
        val mypref = getSharedPreferences(email, Context.MODE_PRIVATE)
       // val PlayerID = mypref.getString(email, "jkl")
        Log.e("Target", PlayerID)


        var url = "https://elimination.azurewebsites.net/api/Targets/GetYourTarget?id="+ PlayerID
        val que = Volley.newRequestQueue(this@Target)
        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null,
                Response.Listener<JSONObject> { response ->
                    //get photo
                    //round photo
                    Glide.with(this)
                            .load(response.get("PhotoUrl"))
                            .apply(RequestOptions.circleCropTransform())
                            .into(ProfilePic)

                    //get UserName
                    userName.text = response.get("UserName").toString()
                    //get profile
                },
                Response.ErrorListener { error -> Log.e("error is ", "" + error) })

        //This is for Headers If Needed
        @Throws(AuthFailureError::class)
        fun getHeaders(): Map<String, String> {
            val params = HashMap<String, String>()
            params.put("Content-Type", "application/json; charset=UTF-8")
            params.put("token", token)
            return params
        }

        que.add(jsonObjectRequest)


    }
}
