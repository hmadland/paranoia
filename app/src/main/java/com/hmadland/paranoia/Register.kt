package com.hmadland.paranoia
import android.Manifest
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import android.util.Base64
import android.content.Context
import android.widget.ProgressBar
import com.android.volley.*
import com.android.volley.DefaultRetryPolicy
import android.content.pm.PackageManager
import android.graphics.drawable.BitmapDrawable
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat

class Register : AppCompatActivity(), View.OnClickListener {
    var  PlayerID = ""
    private var filePath: Uri? = null
    private val PICK_IMAGE_REQUEST = 1
    @RequiresApi(Build.VERSION_CODES.O)


    override fun onClick(v: View?) {
      if(v === buttonLoadPicture)
          showFileChooser()
        else if (v == btn_register)
          uploadFile()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
            if (resultCode != RESULT_CANCELED) {
                if (requestCode == PICK_IMAGE_REQUEST) {
                    val extras = data.extras
                    val imageBitmap = extras!!.get("data") as Bitmap
                   PhotoImageView.setImageBitmap(imageBitmap)
                }
            }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun showFileChooser() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.CAMERA),
                    1)
        }

            val camera =  Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
           camera.putExtra(MediaStore.EXTRA_OUTPUT, filePath)
           startActivityForResult(camera, 1)
    }

    val jsonobject = JSONObject()
    @RequiresApi(Build.VERSION_CODES.O)
    private fun uploadFile() {
        val progress: ProgressBar = progressBarR
        progress.visibility= View.VISIBLE

        val bitmap1 = (PhotoImageView.drawable as BitmapDrawable).getBitmap()
        val stream = ByteArrayOutputStream()
        bitmap1.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val image1 = stream.toByteArray()
        val imageString = Base64.encodeToString(image1,Base64.DEFAULT)

        //get form data from register layout
            jsonobject.put("FirstName", input_fname.text)
            jsonobject.put("LastName", input_lname.text)
            jsonobject.put("UserName", input_username.text)
            jsonobject.put("Phone", input_phone.text)
            jsonobject.put("DOB", input_DOB.text)
            jsonobject.put("Photo", imageString)
            jsonobject.put("Profile", input_profile.text)
            jsonobject.put("Email", input_email.text)
            jsonobject.put("Password", input_password.text)
            jsonobject.put("ConfirmPassword", input_confirm.text)

           var url = "https://elimination.azurewebsites.net/api/Account/Post"

            val que = Volley.newRequestQueue(this@Register)
            val req = JsonObjectRequest(Request.Method.POST, url, jsonobject,
                    Response.Listener<JSONObject>{
                        response -> response.toString()
                        //save PlayerID to val PlayerID
                        PlayerID  =  response.get("PlayerID").toString()
                        //save to sharedPreferences
                        val email = input_email.text.toString()
                        val mypref = getSharedPreferences(email, Context.MODE_PRIVATE)
                        val editor = mypref.edit()
                        editor.putString(email, PlayerID)
                        editor.apply()

                        val intent = Intent(this, Login::class.java)
                        startActivity(intent)
                    },
                    Response.ErrorListener{
                    response ->
                Log.e("Something went wrong", response.toString())
            })
        //if server is slow
        val socketTimeout = 30000 // 30 seconds
        val policy = DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        req.setRetryPolicy(policy)
        // Adding request to request queue
            que.add(req)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        buttonLoadPicture.setOnClickListener(this)
        btn_register.setOnClickListener(this)
    }
}