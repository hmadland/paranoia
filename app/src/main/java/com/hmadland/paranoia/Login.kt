package com.hmadland.paranoia
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_login.*
import com.android.volley.toolbox.StringRequest
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy



class Login : AppCompatActivity() {
     var token = ""
     var email = ""
     var PlayerID = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        log_login.setOnClickListener {
            var url = "https://elimination.azurewebsites.net/token"
            val que = Volley.newRequestQueue(this@Login)
            val stringRequest = object : StringRequest(Request.Method.POST, url,
                    Response.Listener { response -> Toast.makeText(this@Login, response, Toast.LENGTH_LONG) //.show()
                        //set token to response
                        token = response
                        //set email to entered email
                        email = inputLogin_email.text.toString()
                        val intent = Intent(this, Profile::class.java)
                        intent.putExtra("token", token)
                        intent.putExtra("email", email)
                        startActivity(intent)
                    },
                    Response.ErrorListener { error -> Toast.makeText(this@Login, error.toString(), Toast.LENGTH_LONG).show()
                        Log.e("Wrong", " " + error.toString())}) {

                override fun getParams(): Map<String, String> {
                    val params = HashMap<String, String>()
                    params.put("grant_type", "password")
                    params.put("Username", inputLogin_email.toString())
                    params.put("Password", inputLogin_password.toString())
                    return params
                }
            }
            val socketTimeout = 30000 // 30 seconds
            val policy = DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            stringRequest.setRetryPolicy(policy)

            que.add(stringRequest)
        }

    }
}
